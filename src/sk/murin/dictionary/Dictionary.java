package sk.murin.dictionary;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Dictionary {
    private JLabel labelWord;
    private JButton btnSubmit;
    private JTextField field;
    private int position = 0;
    private final Font f = new Font("Consolas", Font.BOLD, 19);
    private final Word[] words = {
            new Word("car", "auto"),
            new Word("beer", "pivo"),
            new Word("gym", "posilnovna"),
            new Word("snow", "sneh"),
            new Word("ball", "lopta"),
            new Word("horse", "kon"),
    };
    private String[] options;

    public Dictionary() {
        setup();
    }

    private void setup() {
        JFrame frame = new JFrame("Slovnik");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new GridLayout(2, 2));
        frame.setSize(500, 200);
        frame.setLocationRelativeTo(null);
        JLabel labelMainMsg = new JLabel(" Prelož do slovenčiny: ");
        labelMainMsg.setFont(f);
        labelWord = new JLabel();
        labelWord.setText(words[0].wordENG);
        labelWord.setFont(f);
        labelWord.setHorizontalAlignment(SwingConstants.CENTER);
        btnSubmit = new JButton("Prelož");
        btnSubmit.setFont(f);
        btnSubmit.setFocusable(false);
        btnSubmit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                check();
            }
        });
        field = new JTextField();
        field.setFont(f);

        options = new String[2];
        options[0] = "Ano";
        options[1] = "Nie";

        frame.add(labelMainMsg);
        frame.add(labelWord);
        frame.add(field);
        frame.add(btnSubmit);
        frame.setResizable(false);
        frame.setVisible(true);

    }

    public void check() {
        String txtFromField = field.getText();

        if (txtFromField.equalsIgnoreCase(words[position].wordSK)) {
            position++;
            System.out.println("ok " + position);
            if (position == words.length) {
                int x = JOptionPane.showOptionDialog(null, "Chceš ísť odznova?", "Rozhodni sa!", 0, JOptionPane.INFORMATION_MESSAGE, null, options, null);
                if (x == JOptionPane.OK_OPTION) {
                    position = 0;
                    labelWord.setText(words[position].wordENG);
                } else {
                    btnSubmit.setEnabled(false);
                }
            } else {
                labelWord.setText(words[position].wordENG);
            }
            field.setText("");

        } else {
            JOptionPane.showMessageDialog(null, "Zle, skús ešte... ");
        }
    }
}
